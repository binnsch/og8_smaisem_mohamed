import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class KindGenerator {

	// Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den
	// Daten eine Liste an Kind-Objekte anlegen

	public static ArrayList<Kind> auslesen() {
		int index=1;
		
		Kind arr[] = new Kind[10000];

		File file = new File("kinder.txt");

		BufferedReader br = null;
		ArrayList<Kind> Arr = new ArrayList<Kind>();
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			while ((line = br.readLine()) != null) {
				line = line.replaceAll(",", "");// filtr
				String[] erg = line.split(" "); 
				// KIndobjekt erstellen
				
				Kind kn = new Kind();
				kn.setBravheitsgrad(Integer.parseInt(erg[3]));
				kn.setGeburtstag(erg[2]);
				kn.setVorname(erg[0]);
				kn.setNachname(erg[1]);

				String ort = "";
				for (int i = 4; i < erg.length; i++) ort += erg[i] + " ";
				kn.setWohnort(ort);
				Arr.add(kn);
				

			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.toString());
		} catch (IOException e) {
			System.out.println("Unable to read file: " + file.toString());
		}

		return Arr;
	}
}
