import java.util.*;

public class Kind implements Comparable {
	
	private String Nachname, Vorname, Geburtstag ,Wohnort;
	private int Bravheitsgrad;
	
	
	public Kind() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Kind(String nachname, String vorname, String geburtstag, String wohnort, int bravheitsgrad) {
		super();
		Nachname = nachname;
		Vorname = vorname;
		Geburtstag = geburtstag;
		Wohnort = wohnort;
		Bravheitsgrad = bravheitsgrad;
	}

	public String getNachname() {
		return Nachname;
	}

	public void setNachname(String nachname) {
		Nachname = nachname;
	}

	public String getVorname() {
		return Vorname;
	}

	public void setVorname(String vorname) {
		Vorname = vorname;
	}

	public String getGeburtstag() {
		return Geburtstag;
	}

	public String getWohnort() {
		return Wohnort;
	}

	public void setWohnort(String wohnort) {
		Wohnort = wohnort;
	}

	public int getBravheitsgrad() {
		return Bravheitsgrad;
	}

	@Override
	public String toString() {
		return "Kind [Nachname=" + Nachname + ", Vorname=" + Vorname + ", Geburtstag=" + Geburtstag + ", Wohnort="
				+ Wohnort + ", Bravheitsgrad=" + Bravheitsgrad + ", getNachname()=" + getNachname() + ", getVorname()="
				+ getVorname() + ", getGeburtstag()=" + getGeburtstag() + ", getWohnort()=" + getWohnort()
				+ ", getBravheitsgrad()=" + getBravheitsgrad() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

	public void setBravheitsgrad(int parseInt) {
		Bravheitsgrad = parseInt;
		
	}

	public void setGeburtstag(String geburtstag) {
		Geburtstag = geburtstag;
	}

	@Override
	
	
	public int compareTo(Object o) {
        Kind other = (Kind) o;
        return new Integer(this.Bravheitsgrad).compareTo  (other.Bravheitsgrad);

		
	}

	
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	//Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode

}
