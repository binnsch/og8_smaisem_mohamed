package quicksort;

import java.util.Arrays;

public class QuickSort {

    private static int[] intarray = { 129, 2, 86, 95, 27, 59, 91, 74, 58, 75, 98, 66, 15, 80, 101, 81, 94, 62, 71, 100 };
    private static int count = 0;
    private int[] quick(int l, int r) {
        int q;
        if (l < r) {
            q = partition(l, r);
            quick(l, q);
            quick(q + 1, r);
        }
        return intarray;
    }

    private int partition(int l, int r) {

        int i, j, x = intarray[(l + r) / 2];
        i = l - 1;
        j = r + 1;
        while (true) {
            do {
                i++;
            } while (intarray[i] < x);

            do {
                j--;
            } while (intarray[j] > x);

            if (i < j) {
                int temp = intarray[i];
                intarray[i] = intarray[j];
                intarray[j] = temp;
                System.out.println(intarray[i] + " mit " + intarray[j] + " getauscht");
                count++;
                
            } else {
                return j;
               
            }
            
        }
    }

    public static void main(String[] args) {
        QuickSort quicksort = new QuickSort();
        int[] arr = quicksort.quick(0, intarray.length - 1);
        System.out.println(Arrays.toString(arr));
        System.out.println(count + " mal getauscht");
    }
}