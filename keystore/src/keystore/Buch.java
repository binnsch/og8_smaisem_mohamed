package keystore;


public class Buch implements Comparable{

private String ISBN;
private String Autor;
private String Titel;

public Buch(String ISBN, String name, String Titel) {
this.ISBN = ISBN;
this.Autor = Autor;
this.Titel = Titel;
}


public String ISBN() {
return ISBN;
}
public String Titel() {
return Titel;
}
public void setPersonalId(String ISBN) {
this.ISBN = ISBN;
}

public String getTitel() {
return Titel;
}
public String getAutor() {
return Autor;
}
public void setAutor(String Autor) {
this.Autor = Autor;
}
public void setTitel(String Titel) {
this.Titel = Titel;
}
public boolean equals(Buch p){
return p.ISBN().equals(this.ISBN);
}

public String toString(){
return "[ ISBN: "+this.ISBN + ", "+ "Autor: "+this.Autor + " Autor: "+this.Titel+" ]";
}

public int compareTo(Buch p){
return this.ISBN.compareTo(p.ISBN());

}


@Override
public int compareTo(Object o) {
	// TODO Auto-generated method stub
	return 0;
}
}