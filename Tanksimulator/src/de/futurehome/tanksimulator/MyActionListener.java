package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

import javax.swing.plaf.synth.SynthScrollBarUI;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 fuellstand = fuellstand + 5;
			 f.myTank.setFuellstand(fuellstand);
			 System.out.println("Tankstand wurde aufgef�llt"+ LocalTime.now()+"F�llstand"   + fuellstand );

			 f.lblFuellstand.setText(""+fuellstand+"%");
		}
		
		
		

		if (obj == f.btnVerbrauchen) {
			 double fuellstand = f.myTank.getFuellstand();
			 fuellstand = fuellstand -f.meinSlider.getValue();
			 f.myTank.setFuellstand(fuellstand);

			 f.lblFuellstand.setText(""+fuellstand);
			 System.out.println("Tankstand wurde um 2L verbraucht"+LocalTime.now()+"F�llstand   "  + fuellstand);
		}
		if (obj == f.btnZurueksetzen) {
			 double fuellstand = f.myTank.getFuellstand();
			 fuellstand =0;
			 f.myTank.setFuellstand(fuellstand);

			 f.lblFuellstand.setText(""+fuellstand+"L " + "fuellstand");
			 System.out.println("Tankstand wurde zur�ckgesetzt"+LocalTime.now() +"F�llstand  "   +  fuellstand);
		}


	}
}