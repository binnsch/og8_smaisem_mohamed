/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci{
   // Konstruktor
 
	   long fiboRekursiv1(int n){
		    if(n<=1)
		      return n;
		    else return fiboRekursiv1(n-1) +fiboRekursiv1(n-2);
   }
   
  /**
    * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
    * @param n 
    * @return die n-te Fibonacci-Zahl
   */
	   long fiboIterativ(int n){
		    long vor = 0, letzt=1 , jetz=0;
		    if (n<=1)
		      return n;
		    else {
		      for (int i =2;i<=n;i++) {
		        jetz =letzt +vor;
		        vor =letzt;
		        letzt =jetz;
		        
		      }
		    }return jetz;

}
}
	

